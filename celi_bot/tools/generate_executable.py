import argparse

parser = argparse.ArgumentParser()

parser.add_argument("root_dir", help="Absolute path of program folder")
parser.add_argument("venv_dir", help="Absolute path of program virtualenv folder")

args = parser.parse_args()

template="""
#!/usr/bin/env bash

PROGRAM_ROOT_DIR="%s"
VIRTUAL_ENV_DIR="%s"
export PROGRAM_NAME=$0

cd ${PROGRAM_ROOT_DIR} && {

    [ ! -d ${VIRTUAL_ENV_DIR} ] && {
        echo "The virtual environment dir[${VIRTUAL_ENV_DIR}] not exists"
        exit
    }

    source ${VIRTUAL_ENV_DIR}/bin/activate && {
        python src/main.py $*
    }
}
"""

bash_scritp = template % ( args.root_dir, args.venv_dir )
print( bash_scritp )
