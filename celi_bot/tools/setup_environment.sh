#!/usr/bin/env bash

main()
{
    local VIRTUAL_ENV_DIR=${1}

    [ ! -d "${VIRTUAL_ENV_DIR}" ] && {
        create_virtual_env ${VIRTUAL_ENV_DIR}
    }

    install_dependencies ${VIRTUAL_ENV_DIR}
}

create_virtual_env()
{
    local venv_dir=${1}
    mkdir ${venv_dir}
    virtualenv --python=python3 --no-site-packages ${venv_dir}
}

install_dependencies()
{
    local venv_dir=${1}
    source ${venv_dir}/bin/activate && {
        pip3 install --upgrade pip
        pip3 install tensorflow
        pip3 install tflearn
        pip3 install hunspell
    }
}

[ -z "$1" ] &&
{
    echo "Usage: ${0} virtualenv_dir"
    exit
}

main $1
