# Celi Bot

## Requisitos:
    * Python3
    * virtualenv
    * Make

## Instalacao(Linux):
    * git clone git@gitlab.com:davi.abreu.w/5v.git
    * cd 5v/celi_bot
    * make && sudo make install
    * celi_bot --start
    * OBS.: Diretorio de instalacao => /opt/celi_bot

## Sem instalacao(Linux):
    * git clone git@gitlab.com:davi.abreu.w/5v.git
    * cd 5v/celi_bot
    * make
    * ./celi_bot --start
    * OBS.: Para visualizar as outras opcoes execute: ./celi_bot --help

