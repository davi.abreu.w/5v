class Bot():

    def __init__(self, cognition, debug=False):
        self.debug = debug
        self.cognition = cognition

    def greeting(self):
        ''' TODO for greeting message
            The plan is to dinamically get the capacities of Celi
            Based on it knowledge base
        '''
        print("Oi, meu nome é Celi ':)' ")
        print()

    def debug_response(self, text):
        print( "#-------------------------------------------------------#" )
        print( "#------------------------Result-------------------------#" )
        print( "#-------------------------------------------------------#" )
        print()
        print( "classificacao--------------------------------------------" )
        print()
        print( self.cognition.classify( text ) )
        print( self.cognition.bow( text, True ) )
        print()
        print( "debug----------------------------------------------------" )
        print()
        print( self.cognition.words )
        print( self.cognition.bow( text, True ) )
        print( "#-------------------------------------------------------#" )
        print()
        elected, suggested = self.cognition.suggest_intent(text)
        print( "elected intent:" )
        print( elected )
        print()
        print( "#-------------------------------------------------------#" )
        print()
        print( "suggested intent:" )
        print( suggested )
        print()
        print( "#-------------------------------------------------------#" )
        print( "#///////////////////////////////////////////////////////#" )
        print()

    def response(self, text):
        elected, suggested = self.cognition.suggest_intent(text)
        print("Imagino que você tinha a intenção de '{}'".format(elected))
        if elected != suggested:
            print("Mas nesse caso é melhor dizer que não entendi.")
        print()

    def run(self):
        while True:
            input_text = input("Fale algo ':)' : ")
            print()
            if self.debug:
                self.debug_response(input_text)
            else:
                self.response(input_text)
            print("---------------------------------")