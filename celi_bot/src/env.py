import os

PROGRAM_NAME = os.environ["PROGRAM_NAME"]
INTENTS_FILE_NAME = "data/celi.intents.json"
TRAINING_FILE_NAME = "cognition_data/training_data.cdata"
MODEL_FILE_NAME = "cognition_data/model.tflearn"
TENSOR_BOARD_DIR = "cognition_data/tflearn_logs"

TRAIN_STEPS = 1000
SHOW_TRAIN = True

NOTHING_INTENT = 'nada'
SURE_RATE_OVER_NOTHING = 0.50

ERROR_THRESHOLD = 0.25

