import env
import random

import lang_utils

# things we need for Tensorflow
import numpy as np
import tflearn
import tensorflow as tf

###LOADING INTENTS
def build_docs(intents_dic):
    ###
    intents = intents_dic
    words = []
    classes = []
    documents = []
    for intent in intents['intents']:
        for pattern in intent['patterns']:
            pattern = lang_utils.strip_points(pattern)
            w = lang_utils.clean_up_sentence(pattern)
            words.extend(w)
            documents.append((w, intent['tag']))
            if intent['tag'] not in classes:
                classes.append(intent['tag'])

    words = sorted(list(set(words)))
    # remove duplicates
    classes = sorted(list(set(classes)))

    print(len(documents), "documents")
    print(len(classes), "classes", classes)
    print(len(words), "unique stemmed words", words)
    return documents, classes, words


def train(documents, classes, words):
    # create our training data
    training = []

    # create an empty array for our output
    output_empty = [0] * len(classes)

    # training set, bag of words for each sentence
    for doc in documents:
        # initialize our bag of words
        bag = []
        # list of tokenized words for the pattern
        pattern_words = doc[0]

        # create our bag of words array
        for w in words:
            bag.append(1) if w in pattern_words else bag.append(0)

        # output is a '0' for each tag and '1' for current tag
        output_row = list(output_empty)
        output_row[classes.index(doc[1])] = 1

        training.append([bag, output_row])

    # shuffle our features and turn into np.array
    random.shuffle(training)
    training = np.array(training)

    # create train and test lists
    train_x = list(training[:, 0])
    train_y = list(training[:, 1])
    return train_x, train_y


def build_network_structure(train_x, train_y):
    # reset underlying graph data
    tf.reset_default_graph()
    # Build neural network
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
    net = tflearn.regression(net)

    # Define model and setup tensorboard
    return tflearn.DNN(net, tensorboard_dir=env.TENSOR_BOARD_DIR)


def build_model(file_name, train_x, train_y):
    model = build_network_structure(train_x, train_y)
    # Start training (apply gradient descent algorithm)
    model.fit(train_x, train_y, n_epoch=env.TRAIN_STEPS, batch_size=8, show_metric=env.SHOW_TRAIN)
    model.save(file_name)
    return train_x, train_y
