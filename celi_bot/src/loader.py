import env
import training
import cognition
import file_handler
import json

def build_cognition():

    def load_model(train_x, train_y):
        model = training.build_network_structure(train_x, train_y)
        model.load(env.MODEL_FILE_NAME)
        return model

    # restore all of our data structures
    classes, words, train_x, train_y = file_handler.load_train_dic(
        env.TRAINING_FILE_NAME)
    model = load_model(train_x, train_y)
    intents = json.load(open(env.INTENTS_FILE_NAME))
    return cognition.Cognition(classes, words, intents, model)
