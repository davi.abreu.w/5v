import random

import numpy as np

import env
import lang_utils


class Cognition():

    def __init__(self, classes, words, intents, model):
        self.classes = classes
        self.words = words
        self.intents = intents
        self.model = model

    # return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
    def bow(self, sentence, show_details=False):
        # tokenize the pattern
        sentence_words = lang_utils.clean_up_sentence(sentence)
        # bag of words
        bag = [0]*len(self.words)
        for s in sentence_words:
            for i,w in enumerate(self.words):
                if w == s:
                    bag[i] = 1
                    if show_details:
                        print ("found in bag: %s" % w)

        return(np.array(bag))

    def classify(self, sentence):
        # generate probabilities from the model
        results = self.model.predict([self.bow(sentence)])[0]
        # filter out predictions below a threshold
        results = [[i,r] for i,r in enumerate(results) if r>env.ERROR_THRESHOLD]
        # sort by strength of probability
        results.sort(key=lambda x: x[1], reverse=True)
        return_list = []
        for r in results:
            return_list.append((self.classes[r[0]], r[1]))
        # return tuple of intent and probability
        return return_list

    def response(self, sentence, userID='123', show_details=False):
        results = self.classify(sentence)
        # if we have a classification then find the matching intent tag
        if results:
            # loop as long as there are matches to process
            while results:
                for i in self.intents['intents']:
                    # find a tag matching the first result
                    if i['tag'] == results[0][0]:
                        # a random response from the intent
                        return random.choice(i['responses'])
                results.pop(0)

    def suggest_intent(self, sentence, userID='123', show_details=False):
        results = self.classify(sentence)
        # if we have a classification then find the matching intent tag
        results_probability = {}
        probability_results = {}
        if results:
            for result in results:
                tag, prob = result
                for i in self.intents['intents']:
                    if i['tag'] == tag:
                        results_probability[tag] = prob
                        probability_results[prob] = tag

        nothing_probability = None
        if env.NOTHING_INTENT in results_probability:
            nothing_probability = results_probability[env.NOTHING_INTENT]

        suggest_intent_prob = max(results_probability.values())
        elected = probability_results[suggest_intent_prob]
        suggested = elected
        if nothing_probability:
            if suggest_intent_prob - nothing_probability < env.SURE_RATE_OVER_NOTHING:
                suggested = env.NOTHING_INTENT
        return elected, suggested
