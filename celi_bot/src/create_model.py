import env
import file_handler
import training
import json

def run():
    intents_dic = json.load(open(env.INTENTS_FILE_NAME))
    documents, classes, words = training.build_docs(intents_dic)
    train_x, train_y = training.train(documents, classes, words)
    train_x, train_y = training.build_model(env.MODEL_FILE_NAME, train_x, train_y)
    training_dic = file_handler.build_train_dic(classes, words, train_x, train_y)
    file_handler.pickle_save(env.TRAINING_FILE_NAME, training_dic)
