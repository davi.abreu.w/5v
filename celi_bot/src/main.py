import argparse
import env

def run_celi(args):
    import loader
    from bot import Bot
    cognition = loader.build_cognition()
    celi = Bot(cognition, args.debug)
    celi.greeting()
    celi.run()

def update_cognition():
    import create_model
    create_model.run()

def main():
    parser = argparse.ArgumentParser(prog=env.PROGRAM_NAME, description="Celi bot command line interface")

    parser.add_argument("-s", "--start", action="store_true")
    parser.add_argument("-u", "--update_cognition", action="store_true")
    parser.add_argument("-d", "--debug", action="store_true")
    args = parser.parse_args()

    if args.update_cognition:
        update_cognition()

    if args.start:
        run_celi(args)

if __name__ == "__main__":
    main()

