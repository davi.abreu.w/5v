import pickle


def pickle_save(file_name, to_encode):
    pickle.dump(to_encode, open(file_name, "wb"))


def pickle_load(file_name):
    return pickle.load(open(file_name, "rb"))


def build_train_dic(classes, words, train_x, train_y):
    return {'words': words,
            'classes': classes,
            'train_x': train_x,
            'train_y': train_y}


def load_train_dic(file_name):
    data = pickle_load(file_name)
    return data['classes'], data['words'], data['train_x'], data['train_y']
