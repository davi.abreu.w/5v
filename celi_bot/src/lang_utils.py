import unicodedata
import hunspell

SC = hunspell.HunSpell('/usr/share/hunspell/pt_BR.dic',
                       '/usr/share/hunspell/pt_BR.aff')


def remove_accent(txt, codif='utf-8'):
    return unicodedata.normalize('NFKD', txt).encode('ASCII', 'ignore').decode('ASCII')


def is_vogel(txt):
    VOGEL = ["a", "e", "i", "o", "u"]
    return remove_accent(txt) in VOGEL


def is_consonant(txt):
    return not is_vogel(txt)


def strip_points(txt):
    REMOVE_LIST = [",", ".", "?", ":", ";", "[", "]", "!"]
    for to_remove in REMOVE_LIST:
        txt = txt.replace(to_remove, "")
    return txt


def exist(txt):
    return SC.spell(txt)


def remove_unknow_words(txt):
    if isinstance(txt, list):
        existant_words = []
        for word in txt:
            if exist(word):
                existant_words.append(word)
        return existant_words
    return txt


def normalize(txt, remove_unknow=True):
    txt = txt.lower()
    txt = strip_points(txt)
    if remove_unknow:
        txt = txt.split()
        txt = remove_unknow_words(txt)
        txt = " ".join(txt)
    txt = remove_accent(txt)
    return txt


def tokenize(txt):
    return txt.split()


def stem(txt):
    txt = SC.stem(txt)
    if (txt):
        return txt[0].decode("latin-1")
    return ""


def stem_list(txt):
    stemmed = []
    for word in txt:
        stemmed.append(stem(word))
    return stemmed

def clean_up_sentence( sentence ):
    # tokenize the pattern
    sentence = strip_points(sentence)
    sentence_words = tokenize( sentence )
    # stem each word
    sentence_words = [ stem( word.lower() ) for word in sentence_words ]
    sentence_words = list( filter( lambda w:  w != "", sentence_words ) )
    return sentence_words
